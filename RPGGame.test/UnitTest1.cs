using System;
using NUnit.Framework;
using RPGGame.Characters;
using RPGGame.CustomExceptions;
using RPGGame.Items;
using RPGGame.Items.Armor;
using RPGGame.Items.Weapon;

namespace RPGGame.test;

public class Tests
{
    private Warrior testWarrior;
    
    private Weapon testAxe;
    private Weapon testBow;

    private Armor testPlateBody;
    private Armor testClothHead;
    
    [SetUp]
    public void Setup()
    {
        testWarrior = new Warrior(1);
        
        // Test weapons
        testAxe = new Weapon("Common axe", 1, 7, 1.1, WeaponType.Axe);
        testBow = new Weapon("Common bow", 1, 12, 0.8, WeaponType.Bow);
        
        // Test armor
        testPlateBody = new Armor("Common bow", ArmorType.Plate, ItemSlot.Body, 1, strength: 1);
        testClothHead = new Armor("Common cloth head armor", ArmorType.Cloth, ItemSlot.Head, 1, intelligence: 5);
    }

    // 1
    [Test]
    public void EquipWeapon_CharacterTooLowLevel_InvalidWeaponExceptionThrown()
    {
        testAxe.LevelUp();
        Assert.Throws<InvalidWeaponException>(() => testWarrior.EquipWeapon(testAxe));
    }

    // 2
    [Test]
    public void EquipArmor_CharacterLevelTooLow_InvalidWeaponException()
    {
        testPlateBody.LevelUp();
        Assert.Throws<InvalidArmorException>(() => testWarrior.EquipArmor(testPlateBody));
    }

    // 3
    [Test]
    public void EquipWeapon_WrongWeaponForCharacter_InvalidWeaponException()
    {
        Assert.Throws<InvalidWeaponException>(() => testWarrior.EquipWeapon(testBow));
    }

    // 4
    [Test]
    public void EquipArmor_WrongArmorForCharacter_InvalidArmorException()
    {
        Assert.Throws<InvalidArmorException>(() => testWarrior.EquipArmor(testClothHead));
    }
    
    // 5
    [Test]
    public void EquipWeapon_ValidWeaponForCharacter_SuccessReturnMessage()
    {
        const string successMessage = "New weapon equipped!";
        var returnMessage = testWarrior.EquipWeapon(testAxe);
        
        Assert.That(returnMessage, Is.EqualTo(successMessage));
    }
    
    // 6
    [Test]
    public void EquipArmor_ValidArmorForCharacter_SuccessReturnMessage()
    {
        const string successMessage = "New armor equipped!";
        var returnMessage = testWarrior.EquipArmor(testPlateBody);
        
        Assert.That(returnMessage, Is.EqualTo(successMessage));
    }
    
    // 7
    [Test]
    public void Damage_NoWeaponEquipped_Only1Damage()
    {
        var noWeaponDamage = testWarrior.Damage;
        var expectedDamage = 1 * (1 + ((double)5 / 100));
        
        Assert.That(noWeaponDamage, Is.EqualTo(expectedDamage));
    }
    
    // 8
    [Test]
    public void AxeDamage_Level1Character_CorrectDamage()
    {
        testWarrior.EquipWeapon(testAxe);

        var actualDamage = testWarrior.Damage;
        var expectedDamage = (7 * (double)1.1) * (1 + (double)5 / 100);
        expectedDamage = Math.Round(expectedDamage, 2);

        Assert.That(actualDamage, Is.EqualTo(expectedDamage));
    }
    
    // 9
    [Test]
    public void WeaponAndArmorDamage_Level1Character_CorrectDamage()
    {
        testWarrior.EquipWeapon(testAxe);
        testWarrior.EquipArmor(testPlateBody);

        var actualDamage = testWarrior.Damage;
        var expectedDamage = (7 * (double)1.1) * (1 + ((5 + 1) / (double)100));
        expectedDamage = Math.Round(expectedDamage, 2);
        
        Assert.That(actualDamage, Is.EqualTo(expectedDamage));
    }
    
    
}