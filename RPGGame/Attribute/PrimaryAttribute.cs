namespace RPGGame;

public class PrimaryAttribute
{
    public int Value { get; private set; }

    /// <summary>
    /// Constructor that takes in the value of the primary attribute
    /// </summary>
    /// <param name="value"></param>
    public PrimaryAttribute(int value)
    {
        Value = value;
    }

    /// <summary>
    /// Increases the value of the attribute by the provided amount.
    /// If the amount is negative, the value remains the same. 
    /// </summary>
    /// <param name="amount">The amount to increase the attribute value by</param>
    public void Add(int amount)
    {
        if (amount < 0)
            return;
        Value += amount;
    }
}