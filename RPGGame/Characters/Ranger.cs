using RPGGame.CustomExceptions;
using RPGGame.Items;
using RPGGame.Items.Armor;
using RPGGame.Items.Weapon;

namespace RPGGame.Characters;

public class Ranger : Character
{
    /// <summary>
    /// Levels up the Ranger
    /// </summary>
    protected override void LevelUp()
    {
        base.LevelUp();
        Strength.Add(1);
        Dexterity.Add(7);
        Intelligence.Add(1);
    }

    /// <summary>
    /// Constructor for Ranger. All primary attributes are set to their appropriate values
    /// </summary>
    public Ranger() : base()
    {
        // All are initialized to 0 in base constructor
        Strength.Add(1);
        Dexterity.Add(7);
        Intelligence.Add(1);
    }
}