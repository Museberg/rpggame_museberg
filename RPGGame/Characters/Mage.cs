using RPGGame.CustomExceptions;
using RPGGame.Items;
using RPGGame.Items.Armor;
using RPGGame.Items.Weapon;

namespace RPGGame.Characters;

public class Mage : Character
{
    /// <summary>
    /// Levels up the Mage
    /// </summary>
    protected override void LevelUp()
    {
        base.LevelUp();
        Strength.Add(1);
        Dexterity.Add(1);
        Intelligence.Add(7);
    }

    /// <summary>
    /// Constructor for Mage. All primary attributes are set to their appropriate values
    /// </summary>
    public Mage() : base()
    {
        // All are initialized to 0 in base constructor
        Strength.Add(1);
        Dexterity.Add(1);
        Intelligence.Add(8);
    }
}