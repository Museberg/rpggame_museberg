using RandomNameGeneratorLibrary;
using System.Linq;
using System.Text;
using RPGGame.CustomExceptions;
using RPGGame.Items;
using RPGGame.Items.Armor;
using RPGGame.Items.Weapon;

namespace RPGGame.Characters;

public abstract class Character
{
    public string Name { get; protected set; }
    public int Level { get; protected set; }

    public Dictionary<ItemSlot, Item> Equipment { get; set; } = new();


    protected PrimaryAttribute Strength { get; set; }
    protected PrimaryAttribute Dexterity { get; set; }
    protected PrimaryAttribute Intelligence { get; set; }
    private int TotalPrimaryAttribute => GetTotalAttributes();
    
    public double Damage => GetDamage();

    /// <summary>
    /// Constructor for character that generates a random name and initializes all the primary attribute
    /// </summary>
    protected Character()
    {
        var personGenerator = new PersonNameGenerator();
        Name = personGenerator.GenerateRandomFirstAndLastName();
        Level = 1;
        
        Strength = new PrimaryAttribute(0);
        Dexterity = new PrimaryAttribute(0);
        Intelligence = new PrimaryAttribute(0);
    }

    /// <summary>
    /// Levels up the character in accordance to their type
    /// </summary>
    protected virtual void LevelUp()
    {
        Level++;
    }

    /// <summary>
    /// Equips the armor if the character supports the armor type and are of high enough level
    /// </summary>
    /// <param name="armor">The armor to equip to the character</param>
    /// <returns>A success message if armor was successfully equipped</returns>
    /// <exception cref="InvalidArmorException">If armor could not be equipped, this exception is thrown</exception>
    public string EquipArmor(Armor armor)
    {
        try
        {
            ArmorHelper.EquipArmor(this, armor);
            return "New armor equipped!";
        }
        catch (InvalidArmorException e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
    }

    /// <summary>
    /// Equips the weapon if the character supports the weapon type and are of high enough level
    /// </summary>
    /// <param name="weapon">The weapon to equip to the character</param>
    /// <returns>A success message if the weapon was successfully equipped</returns>
    /// <exception cref="InvalidWeaponException">If weapon could not be equipped, this exception is thrown</exception>
    public string EquipWeapon(Weapon weapon)
    {
        try
        {
            WeaponHelper.EquipWeapon(this, weapon);
            return "New weapon equipped!";
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    /// <summary>
    /// Gets the damage of the character
    /// </summary>
    /// <returns>A number representing the characters damage</returns>
    private double GetDamage()
    {
        bool hasWeapon = Equipment.ContainsKey(ItemSlot.Weapon);
        // If no weapon, our DPS is simply 1. Else, the DPS is that of the weapon
        var dps = hasWeapon ? ((Weapon)Equipment[ItemSlot.Weapon]).DPS : 1;
        
        var damage = dps * (1 + (double)TotalPrimaryAttribute / (double)100);
        return Math.Round(damage, 2);
    }

    /// <summary>
    /// Returns the total of the characters primary attribute - both the character itself and their armor
    /// </summary>
    /// <returns>A number representing the sum of their primary attribute</returns>
    private int GetTotalAttributes()
    {
        var characterType = this.GetType().Name;
        var attributeSum = 0;
        
        // Getting all equipped armor of character
        List<Armor> equippedArmor = Equipment.Where(e => e.Key != ItemSlot.Weapon)
            .Select(equipment => (Armor)equipment.Value).ToList();

        // Getting sum of primary attribute of character
        switch (characterType)
        {
            case "Mage":
                attributeSum += Intelligence.Value;
                equippedArmor.ForEach(a => attributeSum += a.Intelligence.Value);
                break;
            case "Ranger": case "Rogue":
                attributeSum += Dexterity.Value;
                equippedArmor.ForEach(a => attributeSum += a.Dexterity.Value);
                break;
            case "Warrior":
                attributeSum += Strength.Value;
                equippedArmor.ForEach(a => attributeSum += a.Strength.Value);
                break;
        }

        return attributeSum;
    }

    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.AppendLine($"Name: {Name}");
        sb.AppendLine($"Level: {Level}");
        sb.AppendLine($"Strength: {Strength.Value}");
        sb.AppendLine($"Dexterity: {Dexterity.Value}");
        sb.AppendLine($"Intelligence: {Intelligence.Value}");
        sb.AppendLine($"Damage: {Damage}");

        return sb.ToString();
    }
}