using RPGGame.CustomExceptions;
using RPGGame.Items;
using RPGGame.Items.Armor;
using RPGGame.Items.Weapon;

namespace RPGGame.Characters;

public class Warrior : Character
{
    /// <summary>
    /// Levels up the Warrior
    /// </summary>
    protected override void LevelUp()
    {
        base.LevelUp();
        Strength.Add(3);
        Dexterity.Add(2);
        Intelligence.Add(1);
    }

    /// <summary>
    /// Constructor for Warrior. All primary attributes are set to their appropriate values
    /// </summary>
    public Warrior(int level)
    {
        Level = level;
        // All are initialized to 0 in base constructor
        Strength.Add(5);
        Dexterity.Add(2);
        Intelligence.Add(1);
    }
    
}