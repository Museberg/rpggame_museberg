using RPGGame.CustomExceptions;
using RPGGame.Items;
using RPGGame.Items.Armor;
using RPGGame.Items.Weapon;

namespace RPGGame.Characters;

public class Rogue : Character
{
    /// <summary>
    /// Levels up the Rogue
    /// </summary>
    protected override void LevelUp()
    {
        base.LevelUp();
        Strength.Add(1);
        Dexterity.Add(4);
        Intelligence.Add(1);
    }

    /// <summary>
    /// Constructor for Rogue. All primary attributes are set to their appropriate values
    /// </summary>
    public Rogue() : base()
    {
        // All are initialized to 0 in base constructor
        Strength.Add(2);
        Dexterity.Add(6);
        Intelligence.Add(1);
    }
    
    
}