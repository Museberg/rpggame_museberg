﻿// See https://aka.ms/new-console-template for more information

using System;
using RPGGame.Characters;
using RPGGame.Items;
using RPGGame.Items.Armor;
using RPGGame.Items.Weapon;

namespace RPGGame // Note: actual namespace depends on the project name.
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var testWarrior = new Warrior(1);
        
            // Test weapons
            var testAxe = new Weapon("Common axe", 1, 7, 1.1, WeaponType.Axe);
            var testBow = new Weapon("Common bow", 1, 12, 0.8, WeaponType.Bow);
        
            // Test armor
            var testPlateBody = new Armor("Common bow", ArmorType.Plate, ItemSlot.Body, 1, strength: 1);
            var testClothHead = new Armor("Common cloth head armor", ArmorType.Cloth, ItemSlot.Head, 1, intelligence: 5);
            
            
            testAxe.LevelUp();
            testWarrior.EquipWeapon(testAxe);
        }
    }
}


