namespace RPGGame.Items.Weapon;


public class Weapon : Item
{
    private int BaseDamage { get; set; }
    private double APS { get; set; } // Attacks Per Second
    public double DPS => BaseDamage * APS; // Damage Per Second
    public WeaponType Type { get; set; }

    /// <summary>
    /// Constructor for weapon
    /// </summary>
    /// <param cref="string" name="name">The name of the weapon</param>
    /// <param cref="int" name="requiredLevel">The required level to equip this weapon</param>
    /// <param cref="int" name="baseDamage">The base damage of this weapon</param>
    /// <param cref="double" name="attacksPerSecond">How many attacks per second can be performed by this weapon</param>
    /// <param cref="WeaponType" name="type">The type of this weapon. This dictates who can wield it</param>
    public Weapon(string name, int requiredLevel, int baseDamage, double attacksPerSecond, WeaponType type)
    {
        Name = name;
        RequiredLevel = requiredLevel;
        BaseDamage = baseDamage;
        APS = attacksPerSecond;
        Type = type;
        Slot = ItemSlot.Weapon;
    }
}