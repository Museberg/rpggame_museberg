namespace RPGGame.Items.Weapon;

public enum WeaponType
{
    Axe,
    Bow,
    Dagger,
    Hammer,
    Staff,
    Sword,
    Wand
}