using RPGGame.Characters;
using RPGGame.CustomExceptions;
using RPGGame.Items.Armor;

namespace RPGGame.Items.Weapon;

public static class WeaponHelper
{
    // Used to look up supported weapon types for different characters
    private static readonly Dictionary<string, WeaponType[]> WeaponTypes = new Dictionary<string, WeaponType[]>()
    {
        {"Mage", new[] { WeaponType.Staff, WeaponType.Wand } },
        {"Ranger", new[] { WeaponType.Bow } },
        {"Rogue", new[] { WeaponType.Dagger, WeaponType.Sword } },
        {"Warrior", new[] { WeaponType.Axe, WeaponType.Hammer, WeaponType.Sword }}
    };
    
    /// <summary>
    /// Equips the weapon to a character if they can wield it
    /// </summary>
    /// <param name="character">The character to equip this weapon to</param>
    /// <param name="weapon">The weapon to equip</param>
    /// <exception cref="InvalidWeaponException">Is thrown if character cannot wield the weapon or is too low of a level</exception>
    public static void EquipWeapon(Character character, Weapon weapon)
    {
        // Checking if character can equip weapon
        var validWeaponTypes = WeaponTypes[character.GetType().Name];
        if (!validWeaponTypes.Contains(weapon.Type))
        {
            var exceptionMessage = $"Weapon of type {weapon.Type} cannot be equipped to {character.Name}. " +
                                   $"Only {string.Join(", ", validWeaponTypes)} are supported";
            throw new InvalidWeaponException(exceptionMessage);
        }
        
        // Checking if characters meets the required level for the weapon
        if (character.Level < weapon.RequiredLevel)
        {
            var exceptionMessage =
                $"Character's level is too low for weapon. Weapon requires level of {weapon.RequiredLevel} or more";
            throw new InvalidWeaponException(exceptionMessage);
        }

        // We can now equip weapon into weapon slot
        if (character.Equipment.ContainsKey(ItemSlot.Weapon))
            character.Equipment[ItemSlot.Weapon] = weapon;
        else
            character.Equipment.Add(ItemSlot.Weapon, weapon);
    }
}