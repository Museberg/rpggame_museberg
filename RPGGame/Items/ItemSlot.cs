namespace RPGGame.Items;

public enum ItemSlot
{
    Head,
    Body,
    Legs,
    Weapon
}