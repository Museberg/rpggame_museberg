namespace RPGGame.Items.Armor;

public enum ArmorType
{
    Cloth,
    Leather,
    Mail,
    Plate
}