namespace RPGGame.Items.Armor;

public class Armor : Item
{
    public ArmorType Type { get; set; }
    public PrimaryAttribute Strength { get; private set; }
    public PrimaryAttribute Dexterity { get; private set; }
    public PrimaryAttribute Intelligence { get; private set; }

    /// <summary>
    /// Simple constructor for Armor that lets you set all the values
    /// </summary>
    /// <param cref="string" name="name">Name of the armor</param>
    /// <param cref="ArmorType" name="type">The type of the armor</param>
    /// <param cref="ItemSlot" name="itemSlot">The slot this armor is worn in</param>
    /// <param cref="int" name="requiredLevel">The required level to equip this armor</param>
    /// <param cref="int" name="strength">The value of the primary attribute 'Strength'</param>
    /// <param cref="int" name="dexterity">The value of the primary attribute 'Dexterity'</param>
    /// <param cref="int" name="intelligence">The value of the primary attribute 'Intelligence'</param>
    public Armor(string name, ArmorType type, ItemSlot itemSlot, int requiredLevel, int strength = 0, int dexterity = 0,
        int intelligence = 0)
    {
        Name = name;
        Type = type;
        RequiredLevel = requiredLevel;
        Strength = new PrimaryAttribute(strength);
        Dexterity = new PrimaryAttribute(dexterity);
        Intelligence = new PrimaryAttribute(intelligence);
    }
}