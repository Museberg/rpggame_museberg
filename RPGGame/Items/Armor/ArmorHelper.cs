using RPGGame.Characters;
using RPGGame.CustomExceptions;
using System.Linq;

namespace RPGGame.Items.Armor;

public static class ArmorHelper
{
    // Used to look up supported amor types for the different characters
    private static readonly Dictionary<string, ArmorType[]> ArmorTypes = new Dictionary<string, ArmorType[]>()
    {
        {"Mage", new[] { ArmorType.Cloth } },
        {"Ranger", new[] { ArmorType.Leather, ArmorType.Mail } },
        {"Rogue", new[] { ArmorType.Leather, ArmorType.Mail } },
        {"Warrior", new[] { ArmorType.Mail, ArmorType.Plate }}
    };

    /// <summary>
    /// Equips the armor to the provided character if character supports it
    /// </summary>
    /// <param cref="Character" name="character">The character on which to equip the armor</param>
    /// <param cref="Armor" name="armor">The armor to equip</param>
    /// <exception cref="InvalidArmorException">Is thrown if character is not of right type or high enough level to equip armor</exception>
    public static void EquipArmor(Character character, Armor armor)
    {
        // Checking if character can equip armor
        var validArmorTypes = ArmorTypes[character.GetType().Name];
        if (!validArmorTypes.Contains(armor.Type))
        {
            var exceptionMessage = $"Armor of type {armor.Type} cannot be equipped to {character.Name}. " +
                                   $"Only {string.Join(", ", validArmorTypes)} are supported";
            throw new InvalidArmorException(exceptionMessage);
        }
        
        // Checking if character meets the required level
        if (character.Level < armor.RequiredLevel)
        {
            var exceptionMessage =
                $"Character's level is too low for armor. Armor requires level of {armor.RequiredLevel} or more";
            throw new InvalidArmorException(exceptionMessage);
        }
        
        // Checking if provided item slot is supported for armor
        if (armor.Slot == ItemSlot.Weapon)
        {
            var exceptionMessage =
                $"Armor cannot be equipped into {armor.Slot}. " +
                $"Only {ItemSlot.Body}, {ItemSlot.Head}, and {ItemSlot.Legs} can be used for armor";
            throw new InvalidArmorException(exceptionMessage);
        }
        
        // We can now equip armor into the item slot
        if (character.Equipment.ContainsKey(armor.Slot))
            character.Equipment[armor.Slot] = armor;
        else
            character.Equipment.Add(armor.Slot, armor);
    }
}