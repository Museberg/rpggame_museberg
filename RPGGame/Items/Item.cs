namespace RPGGame.Items;

public abstract class Item
{
    public string Name { get; set; }
    public int RequiredLevel { get; set; }
    public ItemSlot Slot { get; set; }

    /// <summary>
    /// Increases the required level of the item by 1
    /// </summary>
    public void LevelUp()
    {
        RequiredLevel++;
    }
}