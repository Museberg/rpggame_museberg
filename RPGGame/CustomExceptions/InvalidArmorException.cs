namespace RPGGame.CustomExceptions;

[Serializable]
public class InvalidArmorException : Exception
{
    public InvalidArmorException()
    {
        // Do nothing
    }

    public InvalidArmorException(string message) : base(message)
    {
        // Do nothing
    }

    public InvalidArmorException(string message, Exception inner) : base(message, inner)
    {
        // Do nothing
    }
}