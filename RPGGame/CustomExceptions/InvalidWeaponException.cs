namespace RPGGame.CustomExceptions;

[Serializable]
public class InvalidWeaponException : Exception
{
    public InvalidWeaponException()
    {
        // Do nothing
    }

    public InvalidWeaponException(string message) : base(message)
    {
        // Do nothing
    }

    public InvalidWeaponException(string message, Exception inner) : base(message, inner)
    {
        // Do nothing
    }
}